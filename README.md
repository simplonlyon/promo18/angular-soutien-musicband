# Music Band Management

Projet de gestion d'un groupe de musique

## Travail à faire
  1. Créer un projet Angular "MusicBandManagement"
  2. Créer une entité Member {nom, instrument, date arrivée, date départ}, et une entité Band {nom, membres, genre, albums}
  3. a. Créer une composant Band pour afficher les informations sur le groupe
  3. b. Créer un composant MemberList pour gérer la liste des membres du groupe
  4. Créer une page SingleMember qui affiche les informations d'un seul membre