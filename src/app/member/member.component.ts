import { Component, OnInit } from '@angular/core';
import { ActivatedRoute } from '@angular/router';
import { Member } from '../entities';
import { MemberService } from '../member.service';

@Component({
  selector: 'app-member',
  templateUrl: './member.component.html',
  styleUrls: ['./member.component.css']
})
export class MemberComponent implements OnInit {

  member?:Member;
  constructor(private memberService:MemberService, private route:ActivatedRoute) { }

  ngOnInit(): void {
      this.route.params.subscribe(p => this.getMemberById(p['id']));
  }

  getMemberById(id:number) {
    let m = this.memberService.getById(id);
    if (m != null) {
      this.member = m;
    }
  }

}
