import { Injectable } from '@angular/core';
import { Band } from './entities';
import { MemberService } from './member.service';

@Injectable({
  providedIn: 'root'
})
export class BandService {

  band:Band = {
    id: 1,
    name: "P18 All-Stars",
    listMembers: [],
    listAlbums: [ "album1", "album2", "album3"],
    genre: "Hardbass"
  };

  constructor(private memberService:MemberService) { }

  get() {
    this.band.listMembers = this.memberService.getActive();
    return this.band;
  }
}
