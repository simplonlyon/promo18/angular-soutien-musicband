export interface Member {
    id: number;
    name: string;
    instrument: string;
    arrivalDate: Date;
    leaveDate?: Date;
}

export interface Band {
    id: number;
    name: string;
    listMembers: Member[];
    genre: string;
    listAlbums: string[];
}