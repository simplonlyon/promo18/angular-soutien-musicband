import { ThisReceiver } from '@angular/compiler';
import { Injectable } from '@angular/core';
import { Member } from './entities';

@Injectable({
  providedIn: 'root'
})
export class MemberService {

  list:Member[] = [
    {
      id: 1,
      name: "Gérard",
      instrument: "Chant",
      arrivalDate: new Date("2022-01-17")
    },
    {
      id: 3,
      name: "René",
      instrument: "Chant",
      arrivalDate: new Date("2020-04-12"),
      leaveDate: new Date("2022-01-16")
    },
    {
      id: 4,
      name: "André",
      instrument: "Batterie",
      arrivalDate: new Date("2020-08-24")
    }
  ];

  constructor() { }

  getAll() {
    return this.list;
  }
  getById(id:number) {
    let found = false;
    let i:number = 0;
    // Je cherche tant que je n'ai pas trouvé ET il me reste des endroits où chercher
    while(!found && i < this.list.length) {
      if(this.list[i].id == id) {
        found = true;
      } else {
        i++; // i = i+1
      }
    }
    // Sortie du while
    // Cas 1 : j'ai trouvé
    if (found) {
      return this.list[i];
    } else {
    // Cas 2 : je n'ai pas trouvé
      return null;
    }
  }

  // Récupération de la liste des membres du groupe encore présents dans le groupe
  getActive() {
    // Le membre est encore présent dans le groupe s'il n'a pas de date de départ
    return this.list.filter(member => member.leaveDate == null);
  }

  save(member:Member) {
      member.id = this.generateId();
      this.list.push(member);
  }

  generateId() {
    // Hypothèse : le tableau est trié par Id
    // On prend l'id du dernier élément, et on rajoute 1
    return (this.list[this.list.length-1].id + 1);
  }
}
