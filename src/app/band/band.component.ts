import { Component, OnInit } from '@angular/core';
import { BandService } from '../band.service';
import { Band } from '../entities';
import { MemberService } from '../member.service';

@Component({
  selector: 'app-band',
  templateUrl: './band.component.html',
  styleUrls: ['./band.component.css']
})
export class BandComponent implements OnInit {

  band:Band;

  constructor(private bandService:BandService) { 
    this.band = bandService.get();
  }

  ngOnInit(): void {
  }

}
