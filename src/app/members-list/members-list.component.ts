import { Component, OnInit } from '@angular/core';
import { Member } from '../entities';
import { MemberService } from '../member.service';

@Component({
  selector: 'app-members-list',
  templateUrl: './members-list.component.html',
  styleUrls: ['./members-list.component.css']
})
export class MembersListComponent implements OnInit {

  model?:Member = {id: 0, name: "", instrument: "", arrivalDate : new Date("1970-01-01")};

  constructor(private memberService:MemberService) { 
   
  }

  ngOnInit(): void {
  }

  saveMember() {
    this.memberService.save(this.model!);
  }

}
