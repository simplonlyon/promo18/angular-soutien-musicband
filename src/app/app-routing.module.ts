import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { BandComponent } from './band/band.component';
import { MemberComponent } from './member/member.component';
import { MembersListComponent } from './members-list/members-list.component';

const routes: Routes = [
  {path: "members/:id", component:MemberComponent},
  {path: "members", component:MembersListComponent},
  {path: "", component:BandComponent}
];

@NgModule({
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule]
})
export class AppRoutingModule { }
